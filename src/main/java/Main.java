import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public class Main {
    // JDBC driver name and database URL
    static final String DB_URL = "jdbc:postgresql://localhost:5432/company";

    // Database credentials
    static final String USER = "admin";
    static final String PASS = "admin";

    public static void main(String[] args) {
        // Homework code
        // Table was created with:
        // create table salary(id int primary key, employee_id int, amount int);

        LinkedList<Integer> ids = new LinkedList<Integer>(List.of(1, 2, 3));
        LinkedList<Integer> empIds = new LinkedList<Integer>(List.of(101, 102, 103));
        LinkedList<Integer> amounts = new LinkedList<Integer>(List.of(4, 8, 15));
        // Should add all 3 entries
        addSalaries(ids, empIds, amounts);

        ids = new LinkedList<Integer>(List.of(4, 5, 3));
        empIds = new LinkedList<Integer>(List.of(104, 105, 106));
        amounts = new LinkedList<Integer>(List.of(16, 23, 42));
        // Should not add any of 3 entries (because of repeating id)
        addSalaries(ids, empIds, amounts);

        ids = new LinkedList<Integer>(List.of(7, 8, 9));
        empIds = new LinkedList<Integer>(List.of(104, 105, 103));
        amounts = new LinkedList<Integer>(List.of(100, 500, 12345));
        // Should not add any of 3 entries (because of repeating employee_id)
        addSalaries(ids, empIds, amounts);

        // Lab code
        /*Connection conn = null;
        Statement stmt = null;
        try {

            Class.forName("org.postgresql.Driver");
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            conn.setAutoCommit(false);

            System.out.println("Creating statement...");
            stmt = conn.createStatement(
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_UPDATABLE);

            System.out.println("Inserting one row....");
            String SQL = "INSERT INTO Employees " +
                    "VALUES (108, 20, 'Jane', 'Eyre')";
            stmt.executeUpdate(SQL);

            // Commit data here.
            System.out.println("Commiting data here....");
            conn.commit();

            System.out.println("Inserting one row....");
            SQL = "INSERT INTO Employees " +
                    "VALUES (109, 20, 'David', 'Rochester')";
            stmt.executeUpdate(SQL);

            System.out.println("Inserting one row....");
            SQL = "INSERT INTO Employees " +
                    "VALUES (106, 20, 'Rita', 'Tez')";
            stmt.executeUpdate(SQL);

            SQL = "INSERT INTO Employees " +
                    "VALUES (107, 22, 'Sita', 'Singh')";
            stmt.executeUpdate(SQL);

            // Commit data here.
            System.out.println("Committing data here....");
            conn.commit();

            String sql = "SELECT id, first, last, age FROM Employees";
            ResultSet rs = stmt.executeQuery(sql);
            System.out.println("List result set for reference....");
            printRs(rs);
            rs.close();
            stmt.close();
            conn.close();

        } catch (SQLException se) {

            // Handle errors for JDBC
            se.printStackTrace();

            // If there is an error then rollback the changes.
            System.out.println("Rolling back data here....");

            try {
                if (conn != null) conn.rollback();
            } catch (SQLException se2) {
                se2.printStackTrace();
            }

        } catch (Exception e) {

            // Handle errors for Class.forName
            e.printStackTrace();

        } finally {

            // Finally block used to close resources
            try {
                if (stmt != null) stmt.close();
            } catch (SQLException ignored) {}

            try {
                if (conn != null) conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }

        }

        System.out.println("Goodbye!");*/
    }

    public static void printRs(ResultSet rs) throws SQLException {
        // Ensure we start with first row
        rs.beforeFirst();
        while (rs.next()) {
            // Retrieve by column name
            int id = rs.getInt("id");
            int age = rs.getInt("age");
            String first = rs.getString("first");
            String last = rs.getString("last");

            // Display values
            System.out.print("ID: " + id);
            System.out.print(", Age: " + age);
            System.out.print(", First: " + first);
            System.out.println(", Last: " + last);
        }
        System.out.println();
    }

    private static void addSalaries(LinkedList<Integer> ids, LinkedList<Integer> employee_ids, LinkedList<Integer> amount) {
        Connection conn = null;
        Statement stmt = null;

        try {

            System.out.println("Transaction started...");

            Class.forName("org.postgresql.Driver");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            conn.setAutoCommit(false);
            stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);

            for (int i = 0; i < Math.max(ids.size(), Math.max(employee_ids.size(), amount.size())); i++) {
                // Check that there is no such employeeId yet
                String SQL = "SELECT * FROM Salary s WHERE s.employee_id=" + employee_ids.get(i);
                ResultSet res = stmt.executeQuery(SQL);
                if (res.next()) throw new SQLException();

                SQL = "INSERT INTO Salary VALUES (" +
                        ids.get(i) + "," +
                        employee_ids.get(i) + "," +
                        amount.get(i) + ")";
                stmt.executeUpdate(SQL);
            }

            // Commit outside of loop will ensure that the whole
            // transaction will be cancelled in case of rollback
            conn.commit();

            stmt.close();
            conn.close();

            System.out.println("Transaction successfull!");

        } catch (SQLException | IndexOutOfBoundsException se) {

            System.out.println("Transaction failed!");

            // Handle errors for JDBC
            se.printStackTrace();

            // If there is an error then rollback the changes.
            System.out.println("Rolling back the whole transaction...");

            try {
                if (conn != null) conn.rollback();
            } catch (SQLException se2) {
                se2.printStackTrace();
            }

        } catch (Exception e) {

            // Handle errors for Class.forName
            e.printStackTrace();

        } finally {

            // Finally block used to close resources
            try {
                if (stmt != null) stmt.close();
            } catch (SQLException ignored) {}

            try {
                if (conn != null) conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }

        }
    }
}
